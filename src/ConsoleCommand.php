<?php

namespace Nikolajev\ConsoleLite;

class ConsoleCommand
{
    private $arguments = [];

    private $options = [];

    public function __construct(array $argv = [])
    {
        foreach (array_slice($argv, 2) as $parameter) {
            if ($this->isOption($parameter)) {
                list($optionTitle, $optionValue) = array_pad(explode("=", substr($parameter, 2)), 2, null);
                $optionValue = $optionValue === null ? null : trim($optionValue);
                if (in_array($optionValue, ['true', 'false'], true)) {
                    $optionValue = filter_var($optionValue, FILTER_VALIDATE_BOOLEAN);
                }
                $this->options[trim($optionTitle)] = $optionValue;
                continue;
            }

            if ($this->isArgument($parameter)) {
                $this->arguments[] = $parameter;
            }
        }
    }

    protected function getArguments()
    {
        return $this->arguments;
    }

    protected function getArgument(int $position)
    {
        return $this->arguments[$position] ?? null;
    }

    protected function getOptions()
    {
        return $this->options;
    }

    protected function optionIsSet(string $optionTitle)
    {
        return array_key_exists($optionTitle, $this->options);
    }

    protected function getOptionValue(string $optionTitle)
    {
        return $this->options[$optionTitle] ?? null;
    }

    private function isArgument(string $string)
    {
        return substr($string, 0, 2) !== "--";
    }

    private function isOption(string $string)
    {
        return substr($string, 0, 2) === "--";
    }

    public function exec()
    {
        // overwritten
    }
}