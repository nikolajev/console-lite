<?php

namespace Nikolajev\ConsoleLite;

class Factory
{
    private array $argv;

    private string $consoleCommandTitle;

    private array $exceptions;

    private string $namespace;
    
    private array $plugins;
    

    public function __construct(array $argv)
    {
        $this->argv = $argv;
        $this->consoleCommandTitle = $argv[1] ?? null;

        if ($this->consoleCommandTitle === null) {
            echo "No command title specified\n";
            exit;
        }


        // @todo Use
        // Files::json(new File('console-lite'))->get(); Files::json(new File('console-lite'))->put([1,2,3])
        // //$file = new File('console-lite');
        // get(bool $associative = true)
        // //$configJson = Files::json($file);
        // $config = Files::json('console-lite')->get();
        // $configJson->get(); $configJson->put(['colors' => ['warning' => 'red']]); $configJson->merge(['colors' => ['warning' => 'purple']]);
        // $configJson->append()
        $config = json_decode(
            file_get_contents(dirname(__DIR__, 4) . DIRECTORY_SEPARATOR . 'console-lite.json'),
            true
        );
        // @todo $config = Files::json('console-lite')->get();

        $this->namespace = $config['namespace'];
        $this->exceptions = $config['naming exceptions'] ?? [];
        $this->plugins = $config['plugins'] ?? [];
    }

    private function prepareConsoleCommandTitle(string $consoleCommandTitle, string $pluginNamespace = null)
    {
        $namespace = $pluginNamespace ?? $this->namespace;
        
        if(!str_contains($consoleCommandTitle, DIRECTORY_SEPARATOR)){
            return $namespace . ucfirst($consoleCommandTitle);
        }

        $exploded = explode(DIRECTORY_SEPARATOR, $consoleCommandTitle);

        $exploded = array_map(function($value){
            return ucfirst($value);
        }, $exploded);

        return $namespace . implode("\\", $exploded);
    }

    public function getConsoleCommand(): ?ConsoleCommand
    {
        $consoleCommandClassTitle = null;

        if ($this->consoleCommandTitle === '-h') {
            return new HelpCommand();
        }
        
        $pluginNamespace = null;

        if (str_starts_with($this->consoleCommandTitle, "-p=")) {
            $pluginTitle = substr($this->consoleCommandTitle, 3);
            $pluginNamespace = $this->plugins[$pluginTitle];
            $this->consoleCommandTitle = $this->argv[2];
        }

        if (array_key_exists($this->consoleCommandTitle, $this->exceptions)) {
            $consoleCommandClassTitle = $this->namespace . $this->exceptions[$this->consoleCommandTitle];
        }

        $consoleCommandClassTitle = $consoleCommandClassTitle ??  $this->prepareConsoleCommandTitle($this->consoleCommandTitle, $pluginNamespace);
        
        try {
            $commandObject = new $consoleCommandClassTitle($this->argv);
        } catch (\Exception|\Error $e) {
            echo $e->getMessage() . "\n";
            exit;
        }

        return $commandObject;
    }
}