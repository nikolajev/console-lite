# Install 

Ensure your project's `composer.json` includes:

```json
{
  "minimum-stability": "dev",
  "config": {
    "bin-dir": "bin"
  }
}
```

`composer require nikolajev/console-lite`

This will create a `bin/lite` (composer symlink) file inside your project.


# Configure Lite Console

Create `/console-lite.json` file inside your project's root

`namespace` - required

`naming exceptions` - optional

If directory with ConsoleLite command files is inside `src`

```json
{
  "namespace": "App\\ConsoleLite\\"
}
```

If composer autoload psr-4 namespace `ConsoleLite` registered

```json
{
  "namespace": "ConsoleLite\\",
  "naming exceptions": {
    "new": "NewCommand"
  }
}
```

Naming conventions:

`bin/lite create` command will execute `<AnyNamespace\>ConsoleLite\Create` class

`new` word is already reserved in PHP, no option to declare `class New`

In this very case you can specify such an exception inside `naming exceptions` section


# Configure composer

`NB!!!` No need for that if directory with ConsoleLite command files is inside `src`

Inside your project's `composer.json`

```json
{
  "autoload": {
    "psr-4": {
      "ConsoleLite\\": "ConsoleLite/"
    }
  }
}
```

`composer dump-autoload`


# Create Command class file

`/ConsoleLite/NewCommand.php` || `/src/ConsoleLite/NewCommand.php`

```php
<?php

namespace ConsoleLite; // || namespace App\ConsoleLite;

use Nikolajev\ConsoleLite\ConsoleCommand;

class NewCommand extends ConsoleCommand
{
    public function exec()
    {
        $this->getArguments(); // array ['file']
        $this->getArgument(0); // string 'file' 
        $this->getOptions(); // array ['ext' => 'txt']
        $this->optionIsSet('ext'); // bool true
        $this->getOptionValue('ext'); // string 'txt'
        
        // Any execution logics here
    }
}
```

Now you can use `bin/lite new file --ext=txt`